﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SignPost : MonoBehaviour
{
	public void ResetScene() 
	{
        GameManager.Instance.Reset();

        // Reset the scene when the user clicks the sign post
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}