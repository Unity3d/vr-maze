﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum status
{
    idle, solving, solved
}

public class GameManager : MonoBehaviour {

    public static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<GameManager>();
            return _instance;
        }
    }

    public Text TimeToSolveText;
    public Text coinsCollectedText;
    

    // Text
    public float timeToSolve = 0.0f;
    public int coinsCollected = 0;

    public status playerStatus = status.idle;

    void Update()
    {
        if (playerStatus == status.solving)
            timeToSolve += Time.deltaTime;
    }

    public void Reset()
    {
        playerStatus = status.idle;
        coinsCollected = 0;
        timeToSolve = 0.0f;
    }

    public void DisplayPlayerScore()
    {
        playerStatus = status.solved;
        TimeToSolveText.text = "Solved in: " + timeToSolve + " Seconds";
        coinsCollectedText.text = "Coins Collected: " + coinsCollected;
    }

    public void StartedSolving()
    {
        playerStatus = status.solving;
    }
}
